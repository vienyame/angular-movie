import { Component, OnInit, Output, EventEmitter, HostListener, ElementRef, Optional, } from '@angular/core';
import { FormControl , FormGroup } from '@angular/forms';
import { Logger , MoviesService } from '@ngxmovie/services';

@Component({
  selector: 'ngxmovie-movie-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})

export class SearchComponent implements OnInit {

  input = new FormControl();

  searchStr: string;
  inputSearchResults: Array<Object>;

  @Output() searchRes: EventEmitter<Array<Object>> = new EventEmitter();

  @HostListener('mouseenter') onMouseEnter() {
    this.highlight('yellow');
  }
    @HostListener('mouseleave') onMouseLeave() {
    this.highlight(null);
  }
  constructor(private _moviesService: MoviesService, private el: ElementRef, @Optional() private logger: Logger) { }

  ngOnInit() {
    this.input.valueChanges.distinctUntilChanged()
    .subscribe(value =>
    this._moviesService.inputSearch(value)
    .subscribe(results => this.inputSearchResults = results)
    );
  }



  searchMovies()   {
      this._moviesService.searchMovies(this.searchStr).subscribe(res => {
         if ( this.logger) {
      this.logger.log(res.results);
      }
        this.searchRes.emit(res.results);
      })  ;
    }

  private highlight(color: string) {
    this.el.nativeElement.style.backgroundColor = color;
  }

  }



//  On dirait pas une sorte de promesse

  /*this.input.valueChanges
.concatMap(value => this._moviesService.inputSearch(value))
.subscribe(results => this.inputSearchResults = results);
Aplatit le code
 */

/* Encore mieux,
this.input.valueChanges
.filter(query => query.length >= 3) // filter sur la taille de query
.debounceTime(400) // attends 400ms
.distinctUntilChanged() // déclencher une requête seulement si la recherche est différente de la précédente
.switchMap(value => this._moviesService.inputSearch(value))// ne s'interesse qu'à la dernière valeur émise .
.catch(error => Observable. // Ne pas oublier les erreurs , à la moindre erreur le composant s'arrete .
of([])))
.subscribe(results => this.inputSearchResults = results);
*/
