export * from './shared.module';
export * from './movie-card/movie-card.component';
export * from './search/search.component';
export * from './search-results/search-results.component';

