import { Component, Input } from '@angular/core';

@Component({
  selector: 'ngxmovie-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent {
@Input('searchRes') searchRes: Array<Object>;
  constructor() {
    console.log(this.searchRes);
  }

}
