import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Theme } from './../theme';
import { Component, OnInit, Renderer2, ElementRef, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ngxmovie-switch-theme',
  templateUrl: './switch-theme.component.html',
  styleUrls: ['./switch-theme.component.css']
})
export class SwitchThemeComponent implements OnInit {
  selectedTheme: SafeResourceUrl;
  animal = '🐰';
  linktheme: any;
  themes: Theme[];
  defaultTheme = new Theme(
    'superhero',
    'http://bootswatch.com/superhero/bootstrap.min.css'
  );

  constructor(
    private renderer: Renderer2,
    private el: ElementRef,
    private sanitizer: DomSanitizer
  ) {}

  ngOnInit() {
    this.themes = [
      new Theme('cerulean', 'http://bootswatch.com/cerulean/bootstrap.min.css'),
      new Theme('cosmo', 'http://bootswatch.com/cosmo/bootstrap.min.css'),
      new Theme('cyborg', 'http://bootswatch.com/cyborg/bootstrap.min.css'),
      new Theme('flatly', 'http://bootswatch.com/flatly/bootstrap.min.css'),
      new Theme('journal', 'http://bootswatch.com/journal/bootstrap.min.css'),
      new Theme('readable', 'http://bootswatch.com/readable/bootstrap.min.css'),
      new Theme('simplex', 'http://bootswatch.com/simplex/bootstrap.min.css'),
      new Theme('slate', 'http://bootswatch.com/slate/bootstrap.min.css'),
      new Theme('spacelab', 'http://bootswatch.com/spacelab/bootstrap.min.css'),
      new Theme('united', 'http://bootswatch.com/united/bootstrap.min.css'),
      this.defaultTheme
    ];
  }

  LoadTheme(theme?: Theme): void {
    if (!theme) {
      theme = this.defaultTheme;
    }
    this.selectedTheme = this.sanitizer.bypassSecurityTrustResourceUrl(
      theme.url
    );

    /*  try {
      this.linktheme = this.renderer.selectRootElement('.link-theme');
    } catch (e) {
      console.log(e);
    } */
    /*    console.log(this.linktheme);
    if (this.linktheme) { */

    /*   this.renderer.removeChild(this.el.nativeElement, this.linktheme);
      const link = this.renderer.createElement('link');
      this.urlSecure = this.sanitizer.bypassSecurityTrustResourceUrl(theme.url);
      console.log(this.urlSecure);
      this.renderer.setAttribute(
        link,
        'href',
        this.urlSecure
      );
      this.renderer.setAttribute(link, 'rel', 'stylesheet');
      this.renderer.addClass(link, 'link-theme');
      this.renderer.appendChild(this.el.nativeElement, link); */
    // }
  }
}
