import { ThemeInterface } from './theme.model';
export class Theme implements ThemeInterface {
         name: string;
         url: string;
         constructor(name: string, url: string) {
            this.name = name;
            this.url = url;
         }
       }
