import { Component, Inject, LOCALE_ID, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ngxmovie-lang',
  templateUrl: './lang.component.html',
  styleUrls: ['./lang.component.css']
})
export class LangComponent {
  languages = [
    { code: 'en', label: 'English'},
    { code: 'fr', label: 'Français'}
  ];
  constructor( @Inject(LOCALE_ID) public localeId: string) { }
}
