import { InjectionToken } from '@angular/core';

export interface AppConfig {
  apiEndpoint: string;
  title: string;
  APIKEY: string;
}

export const APP_DI_CONFIG: AppConfig = {
  apiEndpoint: 'https://api.themoviedb.org/3/',
  title: 'Dependency Injection',
  APIKEY: '90f2fd5cda5faacff142d061667dbb46'
};

export let APP_CONFIG = new InjectionToken<AppConfig>('app.config');
